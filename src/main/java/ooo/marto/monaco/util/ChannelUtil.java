package ooo.marto.monaco.util;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import ooo.marto.monaco.MonacoDiscord;
import ooo.marto.monaco.config.JsonDiscordConfig;
import ooo.marto.monaco.manager.direct.TicketProgress;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousByteChannel;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.Future;

public class ChannelUtil {

	private final MonacoDiscord monacoDiscord = MonacoDiscord.getInstance();
	private final EmbedUtil embedUtil = monacoDiscord.getEmbedUtil();

	private final JDA jda = MonacoDiscord.getInstance().getJda();

	private final File nextTicketFile = new File("ticket.txt");
	private int nextChannel = 1;

	public ChannelUtil() {
		if (!nextTicketFile.exists()) {
			try {
				InputStream inputStream = JsonDiscordConfig.class.getClassLoader().getResourceAsStream("ticket.txt");
				OutputStream outputStream = new FileOutputStream(nextTicketFile);

				byte[] buffer = new byte[1024];
				int bytesRead;

				while ((bytesRead = inputStream.read(buffer)) > 0) {
					outputStream.write(buffer, 0, bytesRead);
				}

				inputStream.close();
				outputStream.close();
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(nextTicketFile));
			nextChannel = Integer.parseInt(bufferedReader.readLine());
		} catch (FileNotFoundException | NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createTicketChannel(User user, TicketProgress ticketProgress) {
		Category ticketCategory = jda.getCategoryById(ticketProgress.getTicketType().getDiscordId());
		long ticketMember = ticketCategory.getGuild().retrieveMember(user).complete().getIdLong();

		System.out.println("Creating ticket-" + nextChannel + " for " + user.getAsTag() + ".\n");

		TextChannel textChannel = ticketCategory.createTextChannel("ticket-" + nextChannel++)
				.addMemberPermissionOverride(ticketMember,
						Permission.getRaw(Permission.MESSAGE_HISTORY, Permission.MESSAGE_WRITE,
								Permission.MESSAGE_READ), 0L).complete();

		user.openPrivateChannel().complete().sendMessage("Your ticket has been created: " + textChannel.getAsMention())
				.queue(success -> {
					textChannel.sendMessage(embedUtil.createEmbed(user, ticketProgress)).queue();
				});

		ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
		byteBuffer.put(String.valueOf(nextChannel).getBytes());
		byteBuffer.flip();

		try (AsynchronousFileChannel asynchronousFileChannel = AsynchronousFileChannel.open(nextTicketFile.toPath(),
				StandardOpenOption.WRITE)) {
			Future<Integer> future = asynchronousFileChannel.write(byteBuffer, 0);
			while (!future.isDone()) {}

			byteBuffer.clear();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
