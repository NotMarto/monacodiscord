package ooo.marto.monaco.util;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import ooo.marto.monaco.MonacoDiscord;

public class MessageUtil {

	private final JDA jda = MonacoDiscord.getInstance().getJda();

	public boolean sendUserMessage(User user, String message) {
		try {
			PrivateChannel privateChannel = user.openPrivateChannel().complete();
			privateChannel.sendMessage(message).queue();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean sendUserMessage(long userId, String message) {
		try {
			PrivateChannel privateChannel = jda.retrieveUserById(userId).complete().openPrivateChannel().complete();
			privateChannel.sendMessage(message).queue();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
