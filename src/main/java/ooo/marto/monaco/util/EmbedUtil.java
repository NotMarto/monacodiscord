package ooo.marto.monaco.util;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import ooo.marto.monaco.MonacoDiscord;
import ooo.marto.monaco.config.IDiscordConfig;
import ooo.marto.monaco.config.TicketCategory;
import ooo.marto.monaco.manager.direct.TicketProgress;

import java.time.Instant;
import java.util.List;

public class EmbedUtil {

	private final JDA jda = MonacoDiscord.getInstance().getJda();
	private final IDiscordConfig discordConfig = MonacoDiscord.getDiscordConfig();

	public MessageEmbed createEmbed(User user, TicketProgress ticketProgress) {
		EmbedBuilder embedBuilder = new EmbedBuilder();

		TicketCategory ticketCategory = ticketProgress.getTicketType();
		List<String> ticketQuestions = ticketCategory.getQuestions();

		embedBuilder.setTimestamp(Instant.now());
		embedBuilder.setTitle(ticketCategory.getName());
		embedBuilder.setDescription(user.getAsTag() + "'s Ticket");

		for (int i = 0; i < ticketQuestions.size(); i++) {
			embedBuilder.addField(ticketQuestions.get(i), ticketProgress.getAnswers().get(i), false);
		}

		embedBuilder.setColor(discordConfig.getEmbedColor());

		return embedBuilder.build();
	}
}
