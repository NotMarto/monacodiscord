package ooo.marto.monaco.manager.direct;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import ooo.marto.monaco.MonacoDiscord;
import ooo.marto.monaco.config.IDiscordConfig;
import ooo.marto.monaco.config.TicketCategory;
import ooo.marto.monaco.util.ChannelUtil;
import ooo.marto.monaco.util.MessageUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DMHandler {

	private final Map<Long, TicketProgress> progress = new HashMap<>();

	private final MonacoDiscord monacoDiscord = MonacoDiscord.getInstance();
	private final MessageUtil messageUtil = monacoDiscord.getMessageUtil();
	private final ChannelUtil channelUtil = monacoDiscord.getChannelUtil();

	private final IDiscordConfig discordConfig = MonacoDiscord.getDiscordConfig();
	private final String helpMenu;

	public DMHandler() {
		StringBuilder stringBuilder = new StringBuilder();

		for (TicketCategory category : discordConfig.getTicketCategories()) {
			stringBuilder.append("**" + category.getCategoryId() + ".** " + category.getName() + "\n");
		}

		stringBuilder.append("\n*Please respond with the corresponding number.*");
		helpMenu = stringBuilder.toString();
	}

	public void handleDirectMessage(User user, String message) {
		long userId = user.getIdLong();
		if (progress.containsKey(userId)) {
			TicketProgress ticketProgress = progress.get(userId);

			long now = System.currentTimeMillis();
			if ((now - ticketProgress.getLastActive()) / 60000 > discordConfig.getActiveMinutes()) {
				displayHelp(user);
				progress.remove(userId);
				return;
			}

			ticketProgress.setLastActive(now);

			ticketProgress.getAnswers().add(message);
			progressUser(user, ticketProgress);
		} else {
			int requestedQuestionId;

			// Validate input.
			try {
				Integer inputInteger = Integer.parseInt(message);
				requestedQuestionId = inputInteger;
			} catch (Exception e) {
				messageUtil.sendUserMessage(user, "Please enter a valid ticket type.");
				return;
			}

			// Find correct category for ID.
			Optional<TicketCategory> optionalTicketCategory = discordConfig.getTicketCategories().stream()
					.filter(category -> category.getCategoryId() == requestedQuestionId).findFirst();

			if (!optionalTicketCategory.isPresent()) {
				messageUtil.sendUserMessage(user, "Please enter a valid ticket type.");
				return;
			}

			TicketCategory ticketCategory = optionalTicketCategory.get();

			// If message is successful, update progress.
			if (messageUtil.sendUserMessage(user, ticketCategory.getQuestions().get(0)))
				progress.put(userId, new TicketProgress(ticketCategory));
		}
	}

	private void displayHelp(User user) {
		messageUtil.sendUserMessage(user, helpMenu);
	}

	private void progressUser(User user, TicketProgress ticketProgress) {
		TicketCategory category = ticketProgress.getTicketType();
		ticketProgress.setProgress(ticketProgress.getProgress() + 1);

		// User has answered all questions. Create ticket.
		if (ticketProgress.getProgress() >= category.getQuestions().size()) {
			channelUtil.createTicketChannel(user, ticketProgress);
			progress.remove(user.getIdLong());
			return;
		}

		// Send next question
		if (!messageUtil.sendUserMessage(user, category.getQuestions().get(ticketProgress.getProgress()))) {
			progress.remove(user.getIdLong());
		}
	}

}
