package ooo.marto.monaco.manager.direct;

import lombok.Data;
import ooo.marto.monaco.config.TicketCategory;

import java.util.ArrayList;
import java.util.List;

@Data
public class TicketProgress {
	private TicketCategory ticketType;
	private int progress;
	private long lastActive;
	private List<String> answers = new ArrayList<>();

	public TicketProgress(TicketCategory ticketType) {
		this.ticketType = ticketType;
		this.progress = 0;
		this.lastActive = System.currentTimeMillis();
	}
}
