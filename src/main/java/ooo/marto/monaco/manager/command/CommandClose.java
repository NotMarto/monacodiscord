package ooo.marto.monaco.manager.command;

import net.dv8tion.jda.api.entities.*;
import ooo.marto.monaco.MonacoDiscord;
import ooo.marto.monaco.config.AccessLevel;
import ooo.marto.monaco.config.Permission;
import org.apache.commons.collections4.iterators.ReverseListIterator;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;

public class CommandClose extends DiscordCommand {

	public CommandClose(String name, MonacoDiscord monacoDiscord) {
		super(name, monacoDiscord);

		requiredRoleId = discordConfig.getAccessLevelRoleId(discordConfig.getRequiredPermission(Permission.CLOSE));
	}

	@Override public void handleCommand(User user, long channelId, String[] args) {
		Guild guild = jda.getGuildById(discordConfig.getGuildId());
		Member member = guild.retrieveMember(user).complete();
		List<Role> userRoles = member.getRoles();

		boolean permitted = false;

		for (Role role : userRoles) {
			if (role.getIdLong() != requiredRoleId)
				continue;
			permitted = true;
		}

		TextChannel textChannel = jda.getTextChannelById(channelId);

		if (!permitted) {
			textChannel.sendMessage("You do not have permission to use this command.").queue();
			return;
		}

		sendTranscript(textChannel);
	}

	private void sendTranscript(TextChannel textChannel) {
		System.out.println("Closing " + textChannel.getName() + " and uploading transcript.\n");
		textChannel.getHistoryFromBeginning(100).queue(messageHistory -> {
			textChannel.delete().queue();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("TRANSCRIPT FOR " + textChannel.getName() + "\n\n");

			List<Message> retrievedHistory = messageHistory.getRetrievedHistory();
			ReverseListIterator<Message> reverseListIterator = new ReverseListIterator<>(retrievedHistory);

			while (reverseListIterator.hasNext()) {
				Message message = reverseListIterator.next();
				stringBuilder.append("[" + message.getTimeCreated() + "] " + message.getAuthor().getAsTag() + ": " + message.getContentStripped() + "\n");
			}

			InputStream inputStream = new ByteArrayInputStream(stringBuilder.toString().getBytes());
			jda.getTextChannelById(discordConfig.getTranscriptChannelId()).sendFile(inputStream, textChannel.getName() + ".txt").queue();
		});
	}
}
