package ooo.marto.monaco.manager.command;

import net.dv8tion.jda.api.entities.User;
import ooo.marto.monaco.MonacoDiscord;
import ooo.marto.monaco.config.IDiscordConfig;

import java.util.HashMap;
import java.util.Map;

public class CommandManager {

	private final Map<String, DiscordCommand> discordCommands = new HashMap();
	private final MonacoDiscord monacoDiscord = MonacoDiscord.getInstance();
	private final IDiscordConfig discordConfig = MonacoDiscord.getDiscordConfig();
	private final String COMMAND_PREFIX = discordConfig.getPrefix();

	public CommandManager() {
		registerCommand(new CommandAdd(COMMAND_PREFIX + "add", monacoDiscord));
		registerCommand(new CommandClose(COMMAND_PREFIX + "close", monacoDiscord));
		registerCommand(new CommandKick(COMMAND_PREFIX + "kick", monacoDiscord));
		registerCommand(new CommandRename(COMMAND_PREFIX + "rename", monacoDiscord));
	}

	public void handleCommand(User user, long channelId, String message) {
		// Separate base command and arguments from the raw message.
		String[] commandArray = message.split(" ", 2);
		String baseCommand = commandArray[0];

		// If no arguments are present, execute the command without them.
		if (commandArray.length < 2) {
			executeCommand(user, channelId, baseCommand);
			return;
		}

		String[] args = commandArray[1].split(" ");

		executeCommand(user, channelId, baseCommand, args);
	}

	private void executeCommand(User user, long channelId, String name, String... args) {
		if (isCommand(name)) {
			getCommand(name).handleCommand(user, channelId, args);
		}
	}

	private ooo.marto.monaco.manager.command.DiscordCommand getCommand(String name) {
		return discordCommands.get(name);
	}

	private boolean isCommand(String name) {
		return discordCommands.containsKey(name);
	}

	private void registerCommand(ooo.marto.monaco.manager.command.DiscordCommand command) {
		discordCommands.put(command.getName(), command);
	}
}
