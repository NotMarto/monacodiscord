package ooo.marto.monaco.manager.command;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import ooo.marto.monaco.MonacoDiscord;

import java.util.List;

public class CommandAdd extends DiscordCommand {

	public CommandAdd(String name, MonacoDiscord monacoDiscord) {
		super(name, monacoDiscord);

		requiredRoleId = discordConfig.getAccessLevelRoleId(discordConfig.getRequiredPermission(ooo.marto.monaco.config.Permission.ADD));
	}

	@Override public void handleCommand(User user, long channelId, String[] args) {
		Guild guild = jda.getGuildById(discordConfig.getGuildId());
		Member member = guild.retrieveMember(user).complete();
		List<Role> userRoles = member.getRoles();

		boolean permitted = false;

		for (Role role : userRoles) {
			if (role.getIdLong() != requiredRoleId)
				continue;
			permitted = true;
		}

		TextChannel textChannel = jda.getTextChannelById(channelId);

		if (!permitted) {
			textChannel.sendMessage("You do not have permission to use this command.").queue();
			return;
		}

		if (args.length == 0) {
			textChannel.sendMessage("Please specify a user ID.").queue();
			return;
		}

		long targetUserId = 0;

		try {
			targetUserId = Long.parseLong(args[0]);
		} catch (NumberFormatException e) {
			textChannel.sendMessage("Please specify a user ID.").queue();
			return;
		}

		Member targetMember = guild.retrieveMember(jda.retrieveUserById(targetUserId).complete()).complete();

		if (targetMember == null) {
			textChannel.sendMessage("User could not be found.").queue();
			return;
		}

		System.out.println(
				"User " + targetMember.getUser().getAsTag() + " has been added to " + textChannel.getName() + ".");

		textChannel.getManager().putPermissionOverride(targetMember,
				Permission.getRaw(Permission.MESSAGE_WRITE, Permission.MESSAGE_READ, Permission.MESSAGE_HISTORY),
				0L).queue(success -> {
			textChannel.sendMessage(user.getAsMention() + " has been added to this ticket.").queue();
		});
	}
}
