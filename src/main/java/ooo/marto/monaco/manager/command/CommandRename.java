package ooo.marto.monaco.manager.command;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import ooo.marto.monaco.MonacoDiscord;
import ooo.marto.monaco.config.AccessLevel;

import java.util.List;

public class CommandRename extends DiscordCommand {

	public CommandRename(String name, MonacoDiscord monacoDiscord) {
		super(name, monacoDiscord);

		requiredRoleId = discordConfig.getAccessLevelRoleId(discordConfig.getRequiredPermission(ooo.marto.monaco.config.Permission.RENAME));
	}

	@Override public void handleCommand(User user, long channelId, String[] args) {
		Guild guild = jda.getGuildById(discordConfig.getGuildId());
		Member member = guild.retrieveMember(user).complete();
		List<Role> userRoles = member.getRoles();

		boolean permitted = false;

		for (Role role : userRoles) {
			if (role.getIdLong() != requiredRoleId)
				continue;
			permitted = true;
		}

		TextChannel textChannel = jda.getTextChannelById(channelId);

		if (!permitted) {
			textChannel.sendMessage("You do not have permission to use this command.").queue();
			return;
		}

		if (args.length < 1) {
			textChannel.sendMessage("Please specify a new name for this channel.").queue();
			return;
		}

		String newName = "ticket-";

		for (String string : args) {
			newName += (string + " ");
		}

		newName = newName.trim().replaceAll(" ", "-");

		String finalNewName = newName;
		textChannel.getManager().setName(newName).queue(success -> {
			textChannel.sendMessage(String.format("Channel has been renamed to `%s`.", finalNewName)).queue();
		});
	}
}
