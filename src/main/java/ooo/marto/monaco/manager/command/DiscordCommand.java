package ooo.marto.monaco.manager.command;

import lombok.Getter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import ooo.marto.monaco.MonacoDiscord;
import ooo.marto.monaco.config.IDiscordConfig;

public abstract class DiscordCommand {

	@Getter private String name;

	protected JDA jda;
	protected long requiredRoleId;
	protected IDiscordConfig discordConfig;

	public DiscordCommand(String name, MonacoDiscord monacoDiscord) {
		this.name = name;
		this.jda = monacoDiscord.getJda();
		this.discordConfig = MonacoDiscord.getDiscordConfig();
	}

	public abstract void handleCommand(User user, long channelId, String[] args);

}
