package ooo.marto.monaco.listener;

import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import ooo.marto.monaco.MonacoDiscord;
import ooo.marto.monaco.config.IDiscordConfig;
import ooo.marto.monaco.config.TicketCategory;
import ooo.marto.monaco.manager.command.CommandManager;
import ooo.marto.monaco.manager.direct.DMHandler;

import javax.annotation.Nonnull;

public class MessageListener implements EventListener {

	private final MonacoDiscord monacoDiscord = MonacoDiscord.getInstance();
	private final CommandManager commandManager = monacoDiscord.getCommandManager();;
	private final DMHandler dmHandler = monacoDiscord.getDmHandler();

	private final IDiscordConfig discordConfig = MonacoDiscord.getDiscordConfig();

	// Load from config
	private final String COMMAND_PREFIX = discordConfig.getPrefix();
	private final long TICKET_CHANNEL_ID = discordConfig.getTicketChannelId();

	private final String helpMenu;

	public MessageListener() {
		StringBuilder stringBuilder = new StringBuilder();

		for (TicketCategory ticketCategory : discordConfig.getTicketCategories()) {
			stringBuilder.append("**" + ticketCategory.getCategoryId() + ".** " + ticketCategory.getName() + "\n");
		}

		stringBuilder.append("\n*Please respond with the corresponding number.*");

		helpMenu = stringBuilder.toString();
	}

	@Override public final void onEvent(@Nonnull GenericEvent genericEvent) {
		if (genericEvent instanceof MessageReceivedEvent) {
			MessageReceivedEvent messageEvent = (MessageReceivedEvent) genericEvent;
			String content = messageEvent.getMessage().getContentStripped();

			if (messageEvent.getAuthor().isBot())
				return;

			switch (messageEvent.getChannelType()) {
			case PRIVATE:
				dmHandler.handleDirectMessage(messageEvent.getMessage().getAuthor(), content);
				break;
			case TEXT:
				if (messageEvent.getChannel().getIdLong() == TICKET_CHANNEL_ID) {
					messageEvent.getMessage().delete().queue();
					displayHelp(messageEvent.getAuthor());
					return;
				}

				if (!content.startsWith(COMMAND_PREFIX))
					return;

				if (!messageEvent.getChannel().getName().startsWith("ticket-"))
					return;

				commandManager.handleCommand(messageEvent.getAuthor(), messageEvent.getChannel().getIdLong(), content);
				break;
			default:
				return;
			}
		}
	}

	private void displayHelp(User user) {
		monacoDiscord.getMessageUtil().sendUserMessage(user, helpMenu);
	}
}
