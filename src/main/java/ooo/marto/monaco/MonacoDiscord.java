package ooo.marto.monaco;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import ooo.marto.monaco.config.IDiscordConfig;
import ooo.marto.monaco.config.JsonDiscordConfig;
import ooo.marto.monaco.listener.MessageListener;
import ooo.marto.monaco.manager.command.CommandManager;
import ooo.marto.monaco.manager.direct.DMHandler;
import ooo.marto.monaco.util.ChannelUtil;
import ooo.marto.monaco.util.EmbedUtil;
import ooo.marto.monaco.util.MessageUtil;

import javax.annotation.Nonnull;
import javax.security.auth.login.LoginException;
import java.io.*;

public class MonacoDiscord {

	@Getter private static MonacoDiscord instance;

	private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private static final File configFile = new File("config.json");

	public static void main(String[] args) {
		System.out.println("Initializing Monaco Discord Bot...\nLoading Config...\n");
		if (!configFile.exists()) {
			try {
				InputStream inputStream = JsonDiscordConfig.class.getClassLoader().getResourceAsStream("config.json");
				OutputStream outputStream = new FileOutputStream(configFile);

				byte[] buffer = new byte[1024];
				int bytesRead;

				while ((bytesRead = inputStream.read(buffer)) > 0) {
					outputStream.write(buffer, 0, bytesRead);
				}

				inputStream.close();
				outputStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			Reader reader = new BufferedReader(new FileReader(configFile));
			discordConfig = gson.fromJson(reader, JsonDiscordConfig.class);
			discordConfig.init();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		instance = new MonacoDiscord();
	}

	private static void initializeBot(JDA jda) {
		while(instance == null) { }
		instance.init(jda);
	}

	@Getter	private JDA jda;
	@Getter private static IDiscordConfig discordConfig;
	@Getter private CommandManager commandManager;
	@Getter private DMHandler dmHandler;

	@Getter private ChannelUtil channelUtil;
	@Getter private MessageUtil messageUtil;
	@Getter private EmbedUtil embedUtil;

	private MonacoDiscord() {
		System.out.println("Logging into Discord...\n");
		try {
			jda = JDABuilder.createDefault(discordConfig.getLoginToken()).addEventListeners(new EventListener() {
				@Override public void onEvent(@Nonnull GenericEvent genericEvent) {
					if (genericEvent instanceof ReadyEvent) {
						ReadyEvent readyEvent = (ReadyEvent) genericEvent;

						if (discordConfig.isActivationMessageEnabled()) {
							jda.getPresence().setActivity(Activity.playing(discordConfig.getActivityMessage()));
						}

						MonacoDiscord.initializeBot(readyEvent.getJDA());
						System.out.println("\nBot has started successfully.\n");
					}
				}
			}).build();
		} catch (LoginException e) {
			System.out.println("Login failed: " + e.getMessage());
			System.exit(1);
		}
	}

	public void init(JDA jda) {
		this.jda = jda;

		embedUtil = new EmbedUtil();
		channelUtil = new ChannelUtil();
		messageUtil = new MessageUtil();

		commandManager = new CommandManager();
		dmHandler = new DMHandler();
		jda.addEventListener(new MessageListener());
	}

}
