package ooo.marto.monaco.config;

public enum Permission {
	ADD, CLOSE, KICK, RENAME;
}
