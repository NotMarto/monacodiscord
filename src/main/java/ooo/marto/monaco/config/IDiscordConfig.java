package ooo.marto.monaco.config;

import java.util.List;

public interface IDiscordConfig {

	String getPrefix();

	List<TicketCategory> getTicketCategories();

	long getActiveMinutes();
	long getTicketChannelId();
	long getAccessLevelRoleId(AccessLevel accessLevel);
	long getGuildId();
	long getTranscriptChannelId();

	boolean isActivationMessageEnabled();

	String getActivityMessage();
	String getLoginToken();

	AccessLevel getRequiredPermission(Permission permission);

	int getEmbedColor();

	void init();
}
