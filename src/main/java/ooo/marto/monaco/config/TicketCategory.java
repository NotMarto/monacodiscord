package ooo.marto.monaco.config;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class TicketCategory {

	@SerializedName("id") private long categoryId;
	private long discordId;
	private String name;
	private List<String> questions;

}
