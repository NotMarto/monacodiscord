package ooo.marto.monaco.config;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class JsonDiscordConfig implements IDiscordConfig {

	private String loginToken;
	private String prefix;

	private long ticketChannelId;
	private long dmSessionMinutes;

	private List<TicketCategory> categories;

	private String embedColor;

	private long guildId;
	private long transcriptChannelId;

	private boolean enableActivityMessage;
	private String activityMessage;

	private transient int decEmbedColor;

	@SerializedName("roles")
	private Map<AccessLevel, Long> roleAccessMap;

	@SerializedName("commandPrivileges")
	private Map<Permission, AccessLevel> commandPrivileges;

	public void init() {
		StringBuilder stringBuilder = new StringBuilder();
		for (TicketCategory ticketCategory : categories) {
			stringBuilder.append(ticketCategory.getName() + " ");
		}
		System.out
				.printf("Ticket Channel ID: %s\nDM Session Minutes: %s\nCategories: %s\nAdmin Role: %s\nMod Role: %s\nStaff Role: %s\nEmbed Color: %s\nGuild ID: %s\n\nConfig Loaded Successfully!\n",
						ticketChannelId, dmSessionMinutes, stringBuilder.toString(),
						roleAccessMap.get(AccessLevel.ADMIN), roleAccessMap.get(AccessLevel.MOD),
						roleAccessMap.get(AccessLevel.ALL_STAFF), embedColor, guildId);

		decEmbedColor = Integer.parseInt(embedColor, 16);
	}

	@Override public String getPrefix() {
		return prefix;
	}

	@Override public long getActiveMinutes() {
		return dmSessionMinutes;
	}

	@Override public List<TicketCategory> getTicketCategories() {
		return categories;
	}

	@Override public long getTicketChannelId() {
		return ticketChannelId;
	}

	@Override public int getEmbedColor() {
		return decEmbedColor;
	}

	@Override public long getAccessLevelRoleId(AccessLevel accessLevel) {
		return roleAccessMap.get(accessLevel);
	}

	@Override public long getGuildId() {
		return guildId;
	}

	@Override public String getLoginToken() {
		return loginToken;
	}

	@Override public AccessLevel getRequiredPermission(Permission permission) {
		return commandPrivileges.get(permission);
	}

	@Override public long getTranscriptChannelId() {
		return transcriptChannelId;
	}

	@Override public boolean isActivationMessageEnabled() {
		return enableActivityMessage;
	}

	@Override public String getActivityMessage() {
		return activityMessage;
	}
}
