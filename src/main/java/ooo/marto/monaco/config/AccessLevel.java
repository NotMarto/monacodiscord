package ooo.marto.monaco.config;

public enum AccessLevel {

	ALL_STAFF, MOD, ADMIN;

}
